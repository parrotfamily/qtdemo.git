#-------------------------------------------------
#
# Project created by QtCreator 2023-02-27T10:32:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = screenwidget
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    screenwidget.cpp

HEADERS  += mainwindow.h \
    screenwidget.h

FORMS    += mainwindow.ui
