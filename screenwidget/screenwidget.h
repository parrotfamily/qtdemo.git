#ifndef SCREENWIDGET_H
#define SCREENWIDGET_H

#include <QWidget>
#include"qpixmap.h"
#include "QSize"
#include"QPoint"
#include "QMouseEvent"
#include "QMenu"
#include "QContextMenuEvent"
class Screen{
public:
    enum STATUS {SELECT, MOV, SET_W_H};
    Screen() {}
    Screen(QSize size);

    void setStart(QPoint pos);
    void setEnd(QPoint pos);
    QPoint getStart();
    QPoint getEnd();

    QPoint getLeftUp();
    QPoint getRightDown();

    STATUS getStatus();
    void setStatus(STATUS status);

    int width();
    int height();

    //检测坐标点是否在截图区域内
    bool isInArea(QPoint pos);
    //按坐标移动截图区域
    void move(QPoint p);
private:
    //记录 截图区域 左上角、右下角
    QPoint leftUpPos, rightDownPos;
    //记录 鼠标开始位置、结束位置
    QPoint startPos, endPos;
    //记录屏幕大小
    int maxWidth, maxHeight;
    //三个状态: 选择区域、移动区域、设置width height
    STATUS status;

    //比较两位置，判断左上角、右下角
    void cmpPoint(QPoint &s, QPoint &e);
};



class screenwidget : public QWidget
{
    Q_OBJECT
public:
    static screenwidget *Instance();
    explicit screenwidget(QWidget *parent = 0);

signals:
private:
    static QScopedPointer<screenwidget> self;
    QMenu *menu;            //右键菜单对象
    Screen *screen;         //截屏对象
    QPixmap *fullScreen;//保存全屏图像
    QPixmap *bgScreen;      //模糊背景图
    QPoint movPos;          //坐标



protected:
    void contextMenuEvent(QContextMenuEvent *);
    void mousePressEvent(QMouseEvent *);
    void mouseMoveEvent(QMouseEvent *);
    void mouseReleaseEvent(QMouseEvent *);
    void paintEvent(QPaintEvent *);   //绘图
    void showEvent(QShowEvent *);     //绘图前的操作
private slots:
    void saveScreen();
    void saveFullScreen();
    void saveScreenOther();
    void saveFullOther();
};

#endif // SCREENWIDGET_H
