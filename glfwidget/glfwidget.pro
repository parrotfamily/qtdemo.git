#-------------------------------------------------
#
# Project created by QtCreator 2023-02-16T08:36:01
#
#-------------------------------------------------

QT       += core gui



greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = glfwidget
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    gifform.cpp \
    cesi.cpp

HEADERS  += mainwindow.h \
    gifform.h \
    cesi.h \
    gif.h

FORMS    += mainwindow.ui
