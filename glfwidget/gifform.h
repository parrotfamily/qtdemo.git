#ifndef GIFFORM_H
#define GIFFORM_H

#include <QDialog>
#include<QScopedPointer>
#include<QLineEdit>
#include "QLabel"
#include<QTimer>
#include<gif.h>
#include<QColor>
class gifform : public QDialog
{
    Q_OBJECT
    //这个属性的意思是从getBorderWidth读数据写入setBorderWidth
    Q_PROPERTY(int borderWidth READ getBorderWidth WRITE setBorderWidth)
    Q_PROPERTY(QColor bgColor READ getBgColor WRITE setBgColor)
public:
    explicit gifform(QWidget *parent = 0);//构造函数
    static gifform* instance();                  //定义当前类的指针，这句话得意义是写一个单例函数，因为gifform * f=new gifform();按钮被点击窗体重复实例化
protected:
    void resizeEvent(QResizeEvent *);
    bool eventFilter(QObject *watched, QEvent *event);  //事件过滤器，主要是为了自定义窗体之后的移动
    void paintEvent(QPaintEvent *);

private:
    //定义一个智能指针，智能指针窗体关闭自动释放，不影响下一次实例化
    static QScopedPointer<gifform> self;
     QWidget *widgetTop;         //标题栏
     QWidget *widgetMain;        //中间部分
     QWidget *widgetBottom;      //底部栏
     QLineEdit *txtFps;          //帧率输入框
     QLineEdit *txtWidth;        //宽度输入框
     QLineEdit *txtHeight;       //高度输入框
     QPushButton *btnStart;      //开始按钮
     QLabel *labStatus;          //显示状态信息

     int fps;                    //帧数 100为1s
     int borderWidth;            //边框宽度
     QColor bgColor;             //背景颜色

     int count;                  //帧数计数
     QString fileName;           //保存文件名称
     QRect rectGif;              //截屏区域
     QTimer *timer;              //截屏定时器

     Gif gif;                    //gif类对象
     Gif::GifWriter *gifWriter;  //gif写入对象
public:
     int getBorderWidth() const;//获取边框的宽度,从这个里面读取数据
     QColor getBgColor()         const; //从这个里面读
private slots:
    void initControl();   //初始化窗体上控件的代码
    void initForm();
    void saveImage();
    void record();
    void closeAll();
    void resizeForm();    //重置窗体

public Q_SLOTS:
    void setBorderWidth(int borderWidth);
    void setBgColor(const QColor &bgColor);


};

#endif // GIFFORM_H
